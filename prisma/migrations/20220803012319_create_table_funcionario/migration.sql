-- CreateTable
CREATE TABLE `Funcionarios` (
    `id_funcionario` INTEGER NOT NULL AUTO_INCREMENT,
    `nu_cpf` VARCHAR(14) NOT NULL,
    `no_funcionario` VARCHAR(120) NOT NULL,
    `nu_pis` VARCHAR(50) NOT NULL,
    `gn_setor` VARCHAR(50) NOT NULL,
    `nu_telefone` JSON NOT NULL,

    PRIMARY KEY (`id_funcionario`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
