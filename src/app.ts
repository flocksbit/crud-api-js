import cors from 'cors';
import express from 'express'


const app = express();

import { router } from './router/routes';

app.use(express.json())
app.use(cors())

app.use(router)


export { app }