
import { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export const getAll = (async (req: Request, res: Response)  =>  {
    const resp = await prisma.funcionarios.findMany({
        select: {
            id_funcionario: true,
            nu_cpf: true,
            no_funcionario: true,
            gn_setor : true,
        }
    });

    if(resp.length > 0) {
        return res.status(200).json(resp);
    }
    
    return res.status(404).json({"message": "Não há funcionários cadastrados"})
})

export const find = (async (req: Request, res: Response) => {
    const id = parseInt(req.params.id)

    const resp = await prisma.funcionarios.findUnique({
        where: { 
            id_funcionario: id
        }
    })

    if (!resp) {
       return  res.status(404).json({message: 'Funcionário não encontrado' })
    }
    return  res.status(200).json(resp)
})

export const create = (async (req: Request, res: Response) => {
    const resp = await prisma.funcionarios.create({
        data: {
            nu_cpf: req.body.nu_cpf,
            nu_pis: req.body.nu_pis,
            no_funcionario: req.body.no_funcionario,
            gn_setor    : req.body.gn_setor,
            nu_telefone : req.body.nu_telefone,
        }
    })

    if (!resp) {
      return  res.status(404).json({message: 'Não foi possivel cadastrar' })
    }

    return res.status(201).json(resp)
})

export const edit = (async (req: Request, res: Response) => {
    const id = parseInt(req.params.id)

    const resp = await prisma.funcionarios.update({
        where: { 
            id_funcionario: id 
        },
        data: {
            nu_cpf: req.body.nu_cpf,
            no_funcionario: req.body.no_funcionario,
            nu_pis: req.body.nu_pis,
            gn_setor    : req.body.gn_setor,
            nu_telefone : req.body.nu_telefone,
        }
    })

    if (!resp) {
        return res.status(404).json({message: 'Não foi possivel editar' })
    }

    return res.status(200).json(resp)
})

export const destroy = (async (req: Request, res: Response) => {
    const id = parseInt(req.params.id)
    
    const respFind = await prisma.funcionarios.findUnique({
        where: {
            id_funcionario: id
        }
    })

    if(!respFind) {
        return res.status(404).json({message: 'Funcionário não encontrado'})
    }

    const resp = await prisma.funcionarios.delete({
        where: {
          id_funcionario: id
        },
    })


    if (!resp) {
        return res.status(404).json({message: 'Não foi possivel deletar' })
    }

    return res.status(204).json(resp)
})
