import express from 'express'
const router = express.Router()

import { getAll, find, create, edit, destroy } from '../controller/funcionarioController'

router.get('/', getAll)
  
router.get('/:id', find)

router.post('/', create)

router.put('/:id', edit)

router.delete('/:id', destroy)

export { router }